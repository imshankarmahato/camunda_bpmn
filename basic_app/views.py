import time
import logging
import json
import requests
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.status import HTTP_200_OK,HTTP_201_CREATED,HTTP_202_ACCEPTED
from rest_framework.response import Response
from camunda.external_task.external_task import ExternalTask, TaskResult
from camunda.external_task.external_task_worker import ExternalTaskWorker
from camunda.client.engine_client import EngineClient
# from .utils import handle_task

logging.basicConfig(level=logging.INFO)


def handle_task(task: ExternalTask) -> TaskResult:
    import pdb;pdb.set_trace()
    return task.complete({"var2": "value"})


class CamundaMiddle(APIView):
    def get(self,request):

        default_config = {
            "maxTasks": 1,
            "lockDuration": 10000,
            "asyncResponseTimeout": 5000,
            "retries": 3,
            "retryTimeout": 5000,
            "sleepSeconds": 30
        }
        topic_subscribe = ExternalTaskWorker(config=default_config)

 

        topic_subscribe.subscribe('TriggerNotification',handle_task) 
        
        return Response('test')