import time
import logging
import json
import requests
from camunda.external_task.external_task import ExternalTask, TaskResult
from camunda.external_task.external_task_worker import ExternalTaskWorker
from camunda.client.engine_client import EngineClient


# configuration for the Client
def handle_task(task: ExternalTask) -> TaskResult:

    failure, bpmn_error = token_check(), token_check() 
    if failure:
        return task.failure(error_message="task failed",  error_details="failed task details", 
                            max_retries=3, retry_timeout=5000)
    elif bpmn_error:
        return task.bpmn_error(error_cide="BPMN_ERROR_CODE")
    import pdb;pdb.set_trace()
    return task.complete({"var2": "value"}) 

def token_check():
    return "Hello"

   